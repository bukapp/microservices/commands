# Commands

Commands services for Buka



This service will handle the following processes : 

- Transform cart into a command
- Ask the payment service for 



## Installation

```bash
npm install
```

 

## Usage

```bash
npm run build # build ts file to dist

npm run start # start production

npm run dev # Build and run app with nodemon reload
```





## Development



### Structure :

- `src` : source code directory
- `src/index.ts` : Entry-point of the app
- `src/models` : Mongoose models
- `src/modules` : Personal modules
- `src/routes` : Express routers 
- `src/views` : Express views
- `deploy` : k8s yaml scripts 
- `.gitlab-ci.yml` : Gitlab pipeline (do not touch)
- `Dockerfile` : Dockerfile
